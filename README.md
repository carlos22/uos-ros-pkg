
The katana package currently recommends using the diamondback version of ROS with Ubuntu.

* Install ROS: <http://www.ros.org/wiki/diamondback/Installation/Ubuntu>

* Install the uos-ros-pkg (includes the katana package): <http://www.ros.org/wiki/uos-ros-pkg>

* Apply the changes made during this project by executing

		git pull https://bitbucket.org/carlos22/uos-ros-pkg.git master

In the future it may be possible to skip the last step because the changes were already applied upstream.
