Use katana_arm_navigation.launch as the toplevel launch file.

The subdirectories of katana_arm_navigation are meant to mirror the equivalent pr2 packages, e.g.:
katana_arm_navigation/actions is the equivalent of pr2_arm_navigation_actions
